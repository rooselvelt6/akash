defmodule Agni.Repo do
  use Ecto.Repo,
    otp_app: :agni,
    adapter: Ecto.Adapters.Postgres
end
