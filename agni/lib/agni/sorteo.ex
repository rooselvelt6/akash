defmodule Agni.Sorteo do
  @moduledoc """
  The Sorteo context.
  """

  import Ecto.Query, warn: false
  alias Agni.Repo

  alias Agni.Sorteo.Hora

  @doc """
  Returns the list of horas.

  ## Examples

      iex> list_horas()
      [%Hora{}, ...]

  """
  def list_horas do
    Repo.all(Hora)
  end

  

  @doc """
  Gets a single hora.

  Raises `Ecto.NoResultsError` if the Hora does not exist.

  ## Examples

      iex> get_hora!(123)
      %Hora{}

      iex> get_hora!(456)
      ** (Ecto.NoResultsError)

  """
  def get_hora!(id), do: Repo.get!(Hora, id)

  @doc """
  Creates a hora.

  ## Examples

      iex> create_hora(%{field: value})
      {:ok, %Hora{}}

      iex> create_hora(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_hora(attrs \\ %{}) do
    %Hora{}
    |> Hora.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a hora.

  ## Examples

      iex> update_hora(hora, %{field: new_value})
      {:ok, %Hora{}}

      iex> update_hora(hora, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_hora(%Hora{} = hora, attrs) do
    hora
    |> Hora.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a hora.

  ## Examples

      iex> delete_hora(hora)
      {:ok, %Hora{}}

      iex> delete_hora(hora)
      {:error, %Ecto.Changeset{}}

  """
  def delete_hora(%Hora{} = hora) do
    Repo.delete(hora)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking hora changes.

  ## Examples

      iex> change_hora(hora)
      %Ecto.Changeset{data: %Hora{}}

  """
  def change_hora(%Hora{} = hora, attrs \\ %{}) do
    Hora.changeset(hora, attrs)
  end
end
