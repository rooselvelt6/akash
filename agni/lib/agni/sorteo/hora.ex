defmodule Agni.Sorteo.Hora do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "horas" do
    field :bloqueo, :boolean, default: false
    field :estado, Ecto.Enum, values: [:Abierta, :Cerrada]
    field :hora, :string

    timestamps()
  end

  @doc false
  def changeset(hora, attrs) do
    hora
    |> cast(attrs, [:hora, :estado, :bloqueo])
    |> validate_required([:hora, :estado, :bloqueo])
    |> unique_constraint(:hora)
  

  end
end
