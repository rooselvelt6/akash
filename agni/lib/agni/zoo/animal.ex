defmodule Agni.Zoo.Animal do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "animales" do
    field :contador_cierre, :float, default: 0.0
    field :contador_ventas, :float, default: 0.0
    field :estado, Ecto.Enum, values: [:Abierto, :Cerrado], default: :Abierto
    field :figura, :string
    field :nombre, :string
    field :numero, :string
    field :status, :boolean, default: false
    field :valor_cierre, :float, default: 0.0

    timestamps()
  end

  @doc false
  def changeset(animal, attrs) do
    animal
    |> cast(attrs, [:nombre, :numero, :figura, :contador_cierre, :valor_cierre, :contador_ventas, :estado, :status])
    |> validate_required([:nombre, :numero, :figura, :contador_cierre, :valor_cierre, :contador_ventas, :estado, :status])
    |> unique_constraint(:numero)
    |> unique_constraint(:nombre)
  end
end
