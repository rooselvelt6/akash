defmodule AgniWeb.PageController do
  use AgniWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
