defmodule AgniWeb.AnimalLive.Index do
  use AgniWeb, :live_view

  alias Agni.Zoo
  alias Agni.Zoo.Animal

  @impl true
  def mount(_params, _session, socket) do
    {:ok, assign(socket, :animales, list_animales())}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit Animal")
    |> assign(:animal, Zoo.get_animal!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Animal")
    |> assign(:animal, %Animal{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Animales")
    |> assign(:animal, nil)
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    animal = Zoo.get_animal!(id)
    {:ok, _} = Zoo.delete_animal(animal)

    {:noreply, assign(socket, :animales, list_animales())}
  end

  defp list_animales do
    Zoo.list_animales()
  end
end
