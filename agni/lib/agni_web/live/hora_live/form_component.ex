defmodule AgniWeb.HoraLive.FormComponent do
  use AgniWeb, :live_component

  alias Agni.Sorteo

  @impl true
  def update(%{hora: hora} = assigns, socket) do
    changeset = Sorteo.change_hora(hora)

    {:ok,
     socket
     |> assign(assigns)
     |> assign(:changeset, changeset)}
  end

  @impl true
  def handle_event("validate", %{"hora" => hora_params}, socket) do
    changeset =
      socket.assigns.hora
      |> Sorteo.change_hora(hora_params)
      |> Map.put(:action, :validate)

    {:noreply, assign(socket, :changeset, changeset)}
  end

  def handle_event("save", %{"hora" => hora_params}, socket) do
    save_hora(socket, socket.assigns.action, hora_params)
  end

  defp save_hora(socket, :edit, hora_params) do
    case Sorteo.update_hora(socket.assigns.hora, hora_params) do
      {:ok, _hora} ->
        {:noreply,
         socket
         |> put_flash(:info, "Hora updated successfully")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, :changeset, changeset)}
    end
  end

  defp save_hora(socket, :new, hora_params) do
    case Sorteo.create_hora(hora_params) do
      {:ok, _hora} ->
        {:noreply,
         socket
         |> put_flash(:info, "Hora created successfully")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, changeset: changeset)}
    end
  end
end
