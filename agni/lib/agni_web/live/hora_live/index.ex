defmodule AgniWeb.HoraLive.Index do
  use AgniWeb, :live_view

  alias Agni.Sorteo
  alias Agni.Sorteo.Hora

  @impl true
  def mount(_params, _session, socket) do
    {:ok, 
      socket
      |> assign(horas: list_horas())
      |> assign(cantidad_horas: total_horas())
    }
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit Hora")
    |> assign(:hora, Sorteo.get_hora!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Hora")
    |> assign(:hora, %Hora{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Horas")
    |> assign(:hora, nil)
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    hora = Sorteo.get_hora!(id)
    {:ok, _} = Sorteo.delete_hora(hora)

    {:noreply, 
      socket
      |> assign(horas: list_horas())
      |> assign(cantidad_horas: total_horas())
    }
  end

  defp list_horas do
    Sorteo.list_horas()
  end

  defp total_horas do
    Sorteo.list_horas() |> length
  end
end
