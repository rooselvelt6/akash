defmodule Agni.SorteoTest do
  use Agni.DataCase

  alias Agni.Sorteo

  describe "horas" do
    alias Agni.Sorteo.Hora

    import Agni.SorteoFixtures

    @invalid_attrs %{bloqueo: nil, estado: nil, hora: nil}

    test "list_horas/0 returns all horas" do
      hora = hora_fixture()
      assert Sorteo.list_horas() == [hora]
    end

    test "get_hora!/1 returns the hora with given id" do
      hora = hora_fixture()
      assert Sorteo.get_hora!(hora.id) == hora
    end

    test "create_hora/1 with valid data creates a hora" do
      valid_attrs = %{bloqueo: true, estado: :Abierta, hora: "some hora"}

      assert {:ok, %Hora{} = hora} = Sorteo.create_hora(valid_attrs)
      assert hora.bloqueo == true
      assert hora.estado == :Abierta
      assert hora.hora == "some hora"
    end

    test "create_hora/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Sorteo.create_hora(@invalid_attrs)
    end

    test "update_hora/2 with valid data updates the hora" do
      hora = hora_fixture()
      update_attrs = %{bloqueo: false, estado: :Cerrada, hora: "some updated hora"}

      assert {:ok, %Hora{} = hora} = Sorteo.update_hora(hora, update_attrs)
      assert hora.bloqueo == false
      assert hora.estado == :Cerrada
      assert hora.hora == "some updated hora"
    end

    test "update_hora/2 with invalid data returns error changeset" do
      hora = hora_fixture()
      assert {:error, %Ecto.Changeset{}} = Sorteo.update_hora(hora, @invalid_attrs)
      assert hora == Sorteo.get_hora!(hora.id)
    end

    test "delete_hora/1 deletes the hora" do
      hora = hora_fixture()
      assert {:ok, %Hora{}} = Sorteo.delete_hora(hora)
      assert_raise Ecto.NoResultsError, fn -> Sorteo.get_hora!(hora.id) end
    end

    test "change_hora/1 returns a hora changeset" do
      hora = hora_fixture()
      assert %Ecto.Changeset{} = Sorteo.change_hora(hora)
    end
  end
end
