defmodule Agni.ZooTest do
  use Agni.DataCase

  alias Agni.Zoo

  describe "animales" do
    alias Agni.Zoo.Animal

    import Agni.ZooFixtures

    @invalid_attrs %{contador_cierre: nil, contador_ventas: nil, estado: nil, figura: nil, nombre: nil, numero: nil, status: nil, valor_cierre: nil}

    test "list_animales/0 returns all animales" do
      animal = animal_fixture()
      assert Zoo.list_animales() == [animal]
    end

    test "get_animal!/1 returns the animal with given id" do
      animal = animal_fixture()
      assert Zoo.get_animal!(animal.id) == animal
    end

    test "create_animal/1 with valid data creates a animal" do
      valid_attrs = %{contador_cierre: 120.5, contador_ventas: 120.5, estado: :Abierto, figura: "some figura", nombre: "some nombre", numero: "some numero", status: true, valor_cierre: 120.5}

      assert {:ok, %Animal{} = animal} = Zoo.create_animal(valid_attrs)
      assert animal.contador_cierre == 120.5
      assert animal.contador_ventas == 120.5
      assert animal.estado == :Abierto
      assert animal.figura == "some figura"
      assert animal.nombre == "some nombre"
      assert animal.numero == "some numero"
      assert animal.status == true
      assert animal.valor_cierre == 120.5
    end

    test "create_animal/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Zoo.create_animal(@invalid_attrs)
    end

    test "update_animal/2 with valid data updates the animal" do
      animal = animal_fixture()
      update_attrs = %{contador_cierre: 456.7, contador_ventas: 456.7, estado: :Cerrado, figura: "some updated figura", nombre: "some updated nombre", numero: "some updated numero", status: false, valor_cierre: 456.7}

      assert {:ok, %Animal{} = animal} = Zoo.update_animal(animal, update_attrs)
      assert animal.contador_cierre == 456.7
      assert animal.contador_ventas == 456.7
      assert animal.estado == :Cerrado
      assert animal.figura == "some updated figura"
      assert animal.nombre == "some updated nombre"
      assert animal.numero == "some updated numero"
      assert animal.status == false
      assert animal.valor_cierre == 456.7
    end

    test "update_animal/2 with invalid data returns error changeset" do
      animal = animal_fixture()
      assert {:error, %Ecto.Changeset{}} = Zoo.update_animal(animal, @invalid_attrs)
      assert animal == Zoo.get_animal!(animal.id)
    end

    test "delete_animal/1 deletes the animal" do
      animal = animal_fixture()
      assert {:ok, %Animal{}} = Zoo.delete_animal(animal)
      assert_raise Ecto.NoResultsError, fn -> Zoo.get_animal!(animal.id) end
    end

    test "change_animal/1 returns a animal changeset" do
      animal = animal_fixture()
      assert %Ecto.Changeset{} = Zoo.change_animal(animal)
    end
  end
end
