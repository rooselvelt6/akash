defmodule Agni.SorteoFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Agni.Sorteo` context.
  """

  @doc """
  Generate a unique hora hora.
  """
  def unique_hora_hora, do: "some hora#{System.unique_integer([:positive])}"

  @doc """
  Generate a hora.
  """
  def hora_fixture(attrs \\ %{}) do
    {:ok, hora} =
      attrs
      |> Enum.into(%{
        bloqueo: true,
        estado: :Abierta,
        hora: unique_hora_hora()
      })
      |> Agni.Sorteo.create_hora()

    hora
  end
end
