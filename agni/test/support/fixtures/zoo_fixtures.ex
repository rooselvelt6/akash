defmodule Agni.ZooFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Agni.Zoo` context.
  """

  @doc """
  Generate a unique animal nombre.
  """
  def unique_animal_nombre, do: "some nombre#{System.unique_integer([:positive])}"

  @doc """
  Generate a unique animal numero.
  """
  def unique_animal_numero, do: "some numero#{System.unique_integer([:positive])}"

  @doc """
  Generate a animal.
  """
  def animal_fixture(attrs \\ %{}) do
    {:ok, animal} =
      attrs
      |> Enum.into(%{
        contador_cierre: 120.5,
        contador_ventas: 120.5,
        estado: :Abierto,
        figura: "some figura",
        nombre: unique_animal_nombre(),
        numero: unique_animal_numero(),
        status: true,
        valor_cierre: 120.5
      })
      |> Agni.Zoo.create_animal()

    animal
  end
end
