defmodule AgniWeb.HoraLiveTest do
  use AgniWeb.ConnCase

  import Phoenix.LiveViewTest
  import Agni.SorteoFixtures

  @create_attrs %{bloqueo: true, estado: :Abierta, hora: "some hora"}
  @update_attrs %{bloqueo: false, estado: :Cerrada, hora: "some updated hora"}
  @invalid_attrs %{bloqueo: false, estado: nil, hora: nil}

  defp create_hora(_) do
    hora = hora_fixture()
    %{hora: hora}
  end

  describe "Index" do
    setup [:create_hora]

    test "lists all horas", %{conn: conn, hora: hora} do
      {:ok, _index_live, html} = live(conn, Routes.hora_index_path(conn, :index))

      assert html =~ "Listing Horas"
      assert html =~ hora.hora
    end

    test "saves new hora", %{conn: conn} do
      {:ok, index_live, _html} = live(conn, Routes.hora_index_path(conn, :index))

      assert index_live |> element("a", "New Hora") |> render_click() =~
               "New Hora"

      assert_patch(index_live, Routes.hora_index_path(conn, :new))

      assert index_live
             |> form("#hora-form", hora: @invalid_attrs)
             |> render_change() =~ ""

      {:ok, _, html} =
        index_live
        |> form("#hora-form", hora: @create_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.hora_index_path(conn, :index))

      assert html =~ "Hora created successfully"
      assert html =~ "some hora"
    end

    test "updates hora in listing", %{conn: conn, hora: hora} do
      {:ok, index_live, _html} = live(conn, Routes.hora_index_path(conn, :index))

      assert index_live |> element("#hora-#{hora.id} a", "Edit") |> render_click() =~
               "Edit Hora"

      assert_patch(index_live, Routes.hora_index_path(conn, :edit, hora))

      assert index_live
             |> form("#hora-form", hora: @invalid_attrs)
             |> render_change() =~ ""

      {:ok, _, html} =
        index_live
        |> form("#hora-form", hora: @update_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.hora_index_path(conn, :index))

      assert html =~ "Hora updated successfully"
      assert html =~ "some updated hora"
    end

    test "deletes hora in listing", %{conn: conn, hora: hora} do
      {:ok, index_live, _html} = live(conn, Routes.hora_index_path(conn, :index))

      assert index_live |> element("#hora-#{hora.id} a", "Delete") |> render_click()
      refute has_element?(index_live, "#hora-#{hora.id}")
    end
  end

  describe "Show" do
    setup [:create_hora]

    test "displays hora", %{conn: conn, hora: hora} do
      {:ok, _show_live, html} = live(conn, Routes.hora_show_path(conn, :show, hora))

      assert html =~ "Show Hora"
      assert html =~ hora.hora
    end

    test "updates hora within modal", %{conn: conn, hora: hora} do
      {:ok, show_live, _html} = live(conn, Routes.hora_show_path(conn, :show, hora))

      assert show_live |> element("a", "Edit") |> render_click() =~
               "Edit Hora"

      assert_patch(show_live, Routes.hora_show_path(conn, :edit, hora))

      assert show_live
             |> form("#hora-form", hora: @invalid_attrs)
             |> render_change() =~ ""

      {:ok, _, html} =
        show_live
        |> form("#hora-form", hora: @update_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.hora_show_path(conn, :show, hora))

      assert html =~ "Hora updated successfully"
      assert html =~ "some updated hora"
    end
  end
end
