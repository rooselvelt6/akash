defmodule AgniWeb.AnimalLiveTest do
  use AgniWeb.ConnCase

  import Phoenix.LiveViewTest
  import Agni.ZooFixtures

  @create_attrs %{contador_cierre: 120.5, contador_ventas: 120.5, estado: :Abierto, figura: "some figura", nombre: "some nombre", numero: "some numero", status: true, valor_cierre: 120.5}
  @update_attrs %{contador_cierre: 456.7, contador_ventas: 456.7, estado: :Cerrado, figura: "some updated figura", nombre: "some updated nombre", numero: "some updated numero", status: false, valor_cierre: 456.7}
  @invalid_attrs %{contador_cierre: nil, contador_ventas: nil, estado: nil, figura: nil, nombre: nil, numero: nil, status: false, valor_cierre: nil}

  defp create_animal(_) do
    animal = animal_fixture()
    %{animal: animal}
  end

  describe "Index" do
    setup [:create_animal]

    test "lists all animales", %{conn: conn, animal: animal} do
      {:ok, _index_live, html} = live(conn, Routes.animal_index_path(conn, :index))

      assert html =~ "Listing Animales"
      assert html =~ animal.figura
    end

    test "saves new animal", %{conn: conn} do
      {:ok, index_live, _html} = live(conn, Routes.animal_index_path(conn, :index))

      assert index_live |> element("a", "New Animal") |> render_click() =~
               "New Animal"

      assert_patch(index_live, Routes.animal_index_path(conn, :new))

      assert index_live
             |> form("#animal-form", animal: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      {:ok, _, html} =
        index_live
        |> form("#animal-form", animal: @create_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.animal_index_path(conn, :index))

      assert html =~ "Animal created successfully"
      assert html =~ "some figura"
    end

    test "updates animal in listing", %{conn: conn, animal: animal} do
      {:ok, index_live, _html} = live(conn, Routes.animal_index_path(conn, :index))

      assert index_live |> element("#animal-#{animal.id} a", "Edit") |> render_click() =~
               "Edit Animal"

      assert_patch(index_live, Routes.animal_index_path(conn, :edit, animal))

      assert index_live
             |> form("#animal-form", animal: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      {:ok, _, html} =
        index_live
        |> form("#animal-form", animal: @update_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.animal_index_path(conn, :index))

      assert html =~ "Animal updated successfully"
      assert html =~ "some updated figura"
    end

    test "deletes animal in listing", %{conn: conn, animal: animal} do
      {:ok, index_live, _html} = live(conn, Routes.animal_index_path(conn, :index))

      assert index_live |> element("#animal-#{animal.id} a", "Delete") |> render_click()
      refute has_element?(index_live, "#animal-#{animal.id}")
    end
  end

  describe "Show" do
    setup [:create_animal]

    test "displays animal", %{conn: conn, animal: animal} do
      {:ok, _show_live, html} = live(conn, Routes.animal_show_path(conn, :show, animal))

      assert html =~ "Show Animal"
      assert html =~ animal.figura
    end

    test "updates animal within modal", %{conn: conn, animal: animal} do
      {:ok, show_live, _html} = live(conn, Routes.animal_show_path(conn, :show, animal))

      assert show_live |> element("a", "Edit") |> render_click() =~
               "Edit Animal"

      assert_patch(show_live, Routes.animal_show_path(conn, :edit, animal))

      assert show_live
             |> form("#animal-form", animal: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      {:ok, _, html} =
        show_live
        |> form("#animal-form", animal: @update_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.animal_show_path(conn, :show, animal))

      assert html =~ "Animal updated successfully"
      assert html =~ "some updated figura"
    end
  end
end
