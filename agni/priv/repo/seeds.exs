# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Agni.Repo.insert!(%Agni.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
alias Agni.Zoo

animales = [
	%{
		nombre: "Ballena",
		numero: "00",
		figura: "ballena.jpg",
	},

	%{
		nombre: "Delfín",
		numero: "0",
		figura: "delfin.jpg",
	},


	%{
		nombre: "Carnero",
		numero: "1",
		figura: "carnero.jpg",
	},

	%{
		nombre: "Toro",
		numero: "2",
		figura: "toro.jpg",
	},

	%{
		nombre: "Ciempies",
		numero: "3",
		figura: "ciempies.jpg",
	},

	%{
		nombre: "Alacrán",
		numero: "4",
		figura: "alacran.jpg",
	},

	%{
		nombre: "León",
		numero: "5",
		figura: "leon.jpg",
	},

	%{
		nombre: "Rana",
		numero: "6",
		figura: "rana.jpg",
	},

	%{
		nombre: "Perico",
		numero: "7",
		figura: "perico.jpg",
	},

	%{
		nombre: "Ratón",
		numero: "8",
		figura: "raton.jpg",
	},

	%{
		nombre: "Águila",
		numero: "9",
		figura: "aguila.jpg",
	},

	%{
		nombre: "Tigre",
		numero: "10",
		figura: "tigre.jpg",
	},

	%{
		nombre: "Gato",
		numero: "11",
		figura: "gato.jpg",
	},

	%{
		nombre: "Caballo",
		numero: "12",
		figura: "caballo.jpg",
	},

	%{
		nombre: "Mono",
		numero: "13",
		figura: "mono.jpg",
	},

	%{
		nombre: "Paloma",
		numero: "14",
		figura: "paloma.jpg",
	},

	%{
		nombre: "Zorro",
		numero: "15",
		figura: "zorro.jpg",
	},

	%{
		nombre: "Oso",
		numero: "16",
		figura: "oso.jpg",
	},


	%{
		nombre: "Pavo",
		numero: "17",
		figura: "pavo.jpg",
	},

	%{
		nombre: "Burro",
		numero: "18",
		figura: "burro.jpg",
	},

	%{
		nombre: "Chivo",
		numero: "19",
		figura: "chivo.jpg",
	},

	%{
		nombre: "Cochino",
		numero: "20",
		figura: "Cochino.jpg",
	},

	%{
		nombre: "Gallo",
		numero: "21",
		figura: "gallo.jpg",
	},


	%{
		nombre: "Camello",
		numero: "22",
		figura: "camello.jpg",
	},

	%{
		nombre: "Cebra",
		numero: "23",
		figura: "cebra.jpg",
	},

	%{
		nombre: "Iguana",
		numero: "24",
		figura: "iguana.jpg",
	},

	%{
		nombre: "Gallina",
		numero: "25",
		figura: "gallina.jpg",
	},

	%{
		nombre: "Vaca",
		numero: "26",
		figura: "vaca.jpg",
	},

	%{
		nombre: "Perro",
		numero: "27",
		figura: "perro.jpg",
	},


	%{
		nombre: "Zamuro",
		numero: "28",
		figura: "zamuro.jpg",
	},

	%{
		nombre: "Elefante",
		numero: "29",
		figura: "elefante.jpg",
	},

	%{
		nombre: "Caimán",
		numero: "30",
		figura: "caiman.jpg",
	},

	%{
		nombre: "Lapa",
		numero: "31",
		figura: "Lapa.jpg",
	},

	%{
		nombre: "Ardilla",
		numero: "32",
		figura: "ardilla.jpg",
	},

	%{
		nombre: "Pescado",
		numero: "33",
		figura: "pescado.jpg",
	},

	%{
		nombre: "Venado",
		numero: "34",
		figura: "venado.jpg",
	},

	%{
		nombre: "Jirafa",
		numero: "35",
		figura: "jirafa.jpg",
	},

	%{
		nombre: "Culebra",
		numero: "36",
		figura: "culebra.jpg",
	},

]

Enum.each(animales, fn animal -> Zoo.create_animal(animal) end)