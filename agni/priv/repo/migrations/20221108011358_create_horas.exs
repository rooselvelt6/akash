defmodule Agni.Repo.Migrations.CreateHoras do
  use Ecto.Migration

  def change do
    create table(:horas, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :hora, :string
      add :estado, :string
      add :bloqueo, :boolean, default: false, null: false

      timestamps()
    end

    create unique_index(:horas, [:hora])
  end
end
