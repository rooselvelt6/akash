defmodule Agni.Repo.Migrations.CreateAnimales do
  use Ecto.Migration

  def change do
    create table(:animales, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :nombre, :string
      add :numero, :string
      add :figura, :string
      add :contador_cierre, :float
      add :valor_cierre, :float
      add :contador_ventas, :float
      add :estado, :string
      add :status, :boolean, default: false, null: false

      timestamps()
    end

    create unique_index(:animales, [:numero])
    create unique_index(:animales, [:nombre])
  end
end
